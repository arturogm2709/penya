<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->hasMany(\App\User::class);
        //belongsTo
        //hasMany
        //hasOne
        //belongsToMany(en los 2).
        // ejemplo N:M productos con categorias
        //la tabla de la relacion se debe llamar obligatoriamente
        // cathegory_product
        //alfabéticament . cathegory antes que product.
        // product cathegory no funcionaria
    }
}
