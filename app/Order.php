<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;
use Illuminate\Support\Facades\DB;

class Order extends Model
{

    protected $fillable = [
        'paid', 'date', 'user_id'
    ];
    protected $dates = [
        'date'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function products(){
        return $this->belongsToMany(Product::class)->withPivot('product_id','quantity','price');
    }
    public function total($id)
    {
        $pedido = Order::findOrFail($id);
        $total = 0;
        foreach ($this->products as $product){
            $total += $product->pivot->price*$product->pivot->quantity;
        }

        return $total;

        // $precioTotal = 0;
        // $productos = DB::select('SELECT*FROM order_product WHERE order_id = '. $this->id);

        // foreach ($productos as $key => $product) {
        //     $precioTotal += $product->price*$product->quantity;
        // }
        // return $precioTotal;
    }
    public function productosPedidos()
    {
         return $this->belongsToMany(Product::class)->withPivot('product_id','quantity','price');
    }

}
