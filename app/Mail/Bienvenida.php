<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Order;
use App\User;
use PDF;
use Auth;


class Bienvenida extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from ="bienvenida@unMail.com";
        $name="el admin";

        $user = \Auth::user();

        $pdf =PDF::loadView('myPDF', ['order'=> $this->order])->save('pdf');

        $email=$this->view('myPDF')->with('order',$this->order)->from($from,$name)->subject('has realizado un pedido');

        $email->attachData($pdf->output(), "pdf.pdf");

    }
}

