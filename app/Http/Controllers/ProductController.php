<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;


class ProductController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth'); //aplicable a todos los métodos
      //$this->middleware('log')->only('index');//solamente a ....
      //$this->middleware('subscribed')->except('store');//todos excepto ...
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Cathegory::all();
        $products = Product::paginate(10);
        //return $products;

        return view('product.index', ['products' => $products], ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Cathegory::all();

        return view('product.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
            'cathegory_id' => 'required|min:1',

        ];
        $request->validate($rules);
        $product = new Product();
        $product->fill($request->all());
        $product->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     $product = Product::findOrFail($id);
     $categories = Cathegory::all();

     return view('product.show', [
        'product' => $product,
    ],['categories'=>$categories]);
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Cathegory::all();
        $product = Product::findOrFail($id);

        return view('product.edit', ['product' => $product], ['categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = [
        'name' => 'required|max:255|min:3',
        'price' => 'required|numeric',
        'cathegory_id' => 'rmin:1',
    ];
    $request->validate($rules);
    $product = Product::findOrFail($id);
    $product->fill($request->all());
    $product->save();

    return redirect('/products');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return back();
    }
}
