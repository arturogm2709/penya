<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Product;
use PDF;
use App\Mail\Bienvenida;
use Auth;
use Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
     $this->middleware('auth');

 }
     public function generatePDF($id){
/*        $data = ['title' => 'Welcome to HDTuto.com'];

        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('itsolutionstuff.pdf');*/

        //dd($id);
/*        $pedido=Order::findOrFail($id);
        $products = $pedido->pedidosProducto;
        $total = 0;*/

/*         foreach ($products as $product) {
            $total += $product->price*$product->pivot->quantity;
        }*/

/*        $data = [
            'order'=> $pedido,
            'products'=>$products,
            'total'=>$total,
        ];*/

        //$pdf =PDF::loadView('myPDF', $data);

        //return $pdf->stream('pedido')->header('Content-Type', 'application/pdf');
        $pedido = Order::findOrFail($id);
        // dd($pedido->date);
        $productos =$pedido->productosPedidos;
        $total = 0;
        //$pedidos = Order::paginate(10);
        //dd($pedido);

         foreach ($productos as $product) {
            $total += $product->price*$product->pivot->quantity;
        }
        $this->authorize('view', $pedido);
          $data = [
            'cantidad'=>$product->pivot->quantity,
            'total'=>$total,
            'order'=> $pedido,
            'productos'=>$productos,
        ];
        $pdf = PDF::loadView('myPDF', $data);

        //return view('order.show',['pedido' => $pedido], ['productos' => $productos]);
        //return $pdf->download('detallepedido.pdf');
        return $pdf->stream('order')->header('Content-Type', 'application/pdf');

    }
    public function mail($id)
    {
        $order = Order::findOrFail($id);
        $user =Auth::user();
        Mail::to($user->email)->send(new Bienvenida($order));

            return redirect("/order/$id");
    }
 public function index()
 {
    $pedidos = Order::paginate(10);
    //$user = User::findOrFail($id);
        //return $products;
    //$this->authorize('view', $pedidos);

    return view('order.index', ['pedidos' => $pedidos]);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return "estamos en show";
        $pedido = Order::findOrFail($id);
        $productos =$pedido->productosPedidos;
        //$totales = $pedido->total();
        $this->authorize('view', $pedido);

        //$producto = ;

        return view('order.show',['pedido' => $pedido], ['productos' => $productos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = Order::findOrFail($id);

        $this->authorize('edit', $pedido);

        //$total = 0;


        return view('order.edit',['pedido' => $pedido ]);
    }
    public function delete($id)
    {

        $pedido= Order::findOrFail($id);
        Order::findOrFail($id)->productosPedidos()->detach();
        $this->authorize('delete', $pedido);
        $pedido->delete();
        //return "hola";
        return back();
        //return "borrando";
        // $user = User::findOrFail($id);
        // $user->delete();
        // $order = Order::findOrFail($id);
        //  $this->authorize('delete' , $order);
        //  $order->delete();
        // Order::destroy($id);
        // //User::destroy($id);

        // return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //return "hola";
        $pedido = Order::findOrFail($id);
        //return $pedido;
        $pedido->fill($request->all());
        $this->authorize('update', $pedido);

        //$this->authorize('update',$pedido);
        $pedido->save();
        return redirect('/order');

        // $rules = [
        //     'name' => 'required|max:255|min:4',
        //     'email' => "required|unique:users,email,$id,id|max:255|email",
        // ];

        // $request->validate($rules);




        // $user = User::findOrFail($id);
        // $this->authorize('update' , $user);
        // $user->fill($request->all());
        // $user->save();


        // return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
