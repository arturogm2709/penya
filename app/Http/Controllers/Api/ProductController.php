<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Cathegory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
{
    $products = Product::all();
    $products = Product::with('category')->get();
    return $products;
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
{
        //
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
{
    //$product = Product::all();
        // $rules = [
        //     'name' => 'required|max:255|min:3',
        //     'price' => 'required|numeric',
        //     'cathegory_id' => 'required|min:1',

        // ];
        //$request->validate($rules);
        $product = new Product($request->all());
        //$product->fill($request->all());
        $product->save();
        //return response()->json(['ok' => 201], 201);
        return response()->json(['status' => 201]);

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
{
        //$products = Product::all();
    $product = Product::findOrFail($id);
    return $product;
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
{
        //
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
{
        //$product = Product::all();
        // $rules = [
        //     'name' => 'required|max:255|min:3',
        //     'price' => 'required|numeric',
        //     'cathegory_id' => 'required|min:1',

        // ];
        //$request->validate($rules);
        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();


        return response()->json($product, 201);
        //return response()->json($product, 201);
        //return response()->json(['ok' => 201], 201);
        //return response()->json(['status' => 201]);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
{
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json($product, 204);
}
}
