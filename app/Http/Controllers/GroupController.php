<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        $group = $request->session()->get('group');

        if(! $group) {$group = [];}

        return view('group.index', ['users'=>$group]);
        //$request->session()->forget('group');
        //echo "<pre>";
        //var_dump($request->session()->get('group'));
    }
    public function addUser(Request $request, $id)
    {
        //buscar usuario
        $user = User::findOrFail($id);

        $group = $request->session()->get('group');

        if ($group == null ) {
            $group = array();
        }
        $position=-1;
        foreach ($group as $key => $item) {
            if($item->id == $id){
                $item->cantidad++;
                $position = $key;
                break;
            }
        }
        if ($position == -1) {
            $user->cantidad=1;
           $request->session()->push('group', $user);
        }

        return redirect('/groups');
        // $group = Session::get('group');
        // if (! $group) {
        //     $group =  array();
        //     session::put($group);
        // }
        //$session = Session::all();
        //dd($session);
        //comprobar si esta en grupo de sesion
        // si no esta se añade
    //return "guardar usuario $id";
    }
    public function flush(Request $request)
    {
        $request->session()->forget('group');
        return back();
    }
}
