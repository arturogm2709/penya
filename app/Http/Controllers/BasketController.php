<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use App\Product;
use App\Cathegory;
use Carbon\Carbon;
use App\Order;
Use App\Mail;
//use date(format);

class BasketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
     $this->middleware('auth');

 }
 public function index(Request $request)
 {
    $total2=0;
    $total= 0;
    $categories = Cathegory::all();
    $basket = $request->session()->get('basket');

    if(! $basket) {$basket = [];}
        // return view('basket.index', ['products' => $basket]) ;

    return view('basket.index', ['products' => $basket, 'categories' => $categories, 'total' => $total, 'total2' =>$total2]) ;
        //$request->session()->forget('group');
        //echo "<pre>";
        //var_dump($request->session()->get('group'));
}
public function saveBasket()
{

}
public function saveOrder()
{
    $date = Carbon::now();
    return $date;
}
public static function total()
{
    $basket= Session::get('basket');
    if ($basket == null ) {
        $basket = array();
        return '';
    }
    $cantidadTotal = 0;


    foreach ($basket as $key ) {
        $numProducts=$key->cantidad;
        $cantidadTotal+=$numProducts;
    }
    return $cantidadTotal;


       //   count ( mixed $$basket [, int $mode = COUNT_NORMAL ] ) : int
       // $basket->length
        //return ($cantidadTotal);
}
public function addProduct(Request $request, $id)
{
    $product = Product::findOrFail($id);

    $basket = $request->session()->get('basket');

    if ($basket == null ) {
        $basket = array();
    }
    $position=-1;
    foreach ($basket as $key => $item) {
        if($item->id == $id){
            $item->cantidad++;
            $position = $key;
            break;
        }
    }
    if ($position == -1) {
        $product->cantidad=1;
        $request->session()->push('basket', $product);
    }

    return back();
        // $group = Session::get('group');
        // if (! $group) {
        //     $group =  array();
        //     session::put($group);
        // }
        //$session = Session::all();
        //dd($session);
        //comprobar si esta en grupo de sesion
        // si no esta se añade
    //return "guardar usuario $id";
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function flush(Request $request)
    {
        $request->session()->forget('basket');
        return back();
    }
    public function create()
    {
        //
    }
    public function delete(Request $request,$id)
    {//para borrar un elemento de Sesion
        $product=Product::findOrFail($id);
        $basket=$request->session()->get('basket');

        foreach ($basket as $key => $productSesion){
        //productSesion son los usuarios que actualmente estan la group/
            if ($product->id==$basket[$key]->id){
                if ($productSesion->cantidad <=1){
                    $request->session()->forget('basket.' .$key);
                }else{
                    $productSesion->cantidad--;
                }
                return back();
            }
        }
        return back();
    }//fin delete´

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

/*    $total2=0;
    $total= 0;
    $categories = Cathegory::all();
    //$basket = $request->session()->get('basket');

        $order = New Order;
        $basket = $request->session()->get('basket');
        $order->paid = 0;
        $order->date = Carbon::today();
        $order->user_id = Auth()->user()->id;

        $order->save();

        foreach ($basket as $products) {
          $order->products()->attach($products->id,
            [
              'price' => $products->price,
              'quantity' => $products->cantidad,
              'date' => $products->date
          ]);
      }
    $orderController = new OrderController;
    $orderController->mail($order->id);
      return redirect("/basket/flush");*/
        $order = New Order;
        $basket = $request->session()->get('basket');
        $order->paid = 0;
        $order->date = Carbon::now();
        $order->user_id = Auth()->user()->id;

        $order->save();
        $total = "";

        foreach ($basket as $products) {
          $order->products()->attach($products->id,
            [
              'price' => $products->price,
              'quantity' => $products->cantidad,
          ]);
      }

    $orderController = new OrderController;
    $orderController->mail($order->id);



      return redirect("/basket/flush");

  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
