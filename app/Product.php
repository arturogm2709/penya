<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
      protected $fillable = [
        'name','price','cathegory_id'
    ];
    public function category()
    {
        return $this->belongsTo(\App\Cathegory::class);
        //belongsTo
        //hasMany
        //hasOne
        //belongsToMany(en los 2)
    }
}
