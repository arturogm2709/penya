@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de roles</h1>
      <div class="alert">
        <a href="/roles/create" class="btn btn-primary">Nuevo</a>
      </div>

      <table  class="table table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
              <th>Operaciones</th>
            </tr>
          </thead>


          <tbody>


            @forelse ($roles as $role)
            <tr>
              <td>{{ $role->name }}</td>

              <td>

                <form method="post" action="/roles/{{ $role->id }}">

                @can ('update' , $role)
                <a class="btn btn-primary"  role="button"
                  href="/roles/{{ $role->id }}/edit">
                  Editar
                </a>
                @endcan
                @can('view', $role)
                <a class="btn btn-primary"  role="button"
                href="/roles/{{ $role->id }}">
                Ver
              </a>
              @endcan
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              @can('delete', $role)
              <input  type="submit" value="borrar" class="btn btn-primary">
              @endcan
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>

    {{ $roles->render() }}
  </div>
</div>
</div>
@endsection
