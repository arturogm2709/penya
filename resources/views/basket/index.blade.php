@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Cesta de productos</h1>

      <table  class="table table-hover">
        <thead>
          <tr>
            <th><b>Nombre</th>
              <th><b>Categoría</th>
                <th><b>Precio</th>
                  <th><b>Cantidad</th>
                    <th><b>Precio de campo</b>
                    </tr>
                  </thead>


                  <tbody>


                    @forelse ($products as $product)
                    @foreach ($categories as $category)
                    @if($product->cathegory_id == $category->id)
                    <tr>
                      <td>{{ $product->name }}</td>
                      <td>{{ $category->name}}</td>
                      <td>{{ $product->price }} €</td>
                      <td>{{ $product->cantidad }}</td>
                      <td>{{ $total = $product->cantidad * $product->price }} €</td>
                      <td>
                        <a href="/basket/{{$product->id}}" class="btn btn-primary" color="white">+</a>
                        <a  href="/basket/delete/{{ $product->id }}" class="btn btn-primary">-</a></td>
                      </td>

                      <td>

                        <a href="/products/{{$product->id}}" class="btn btn-primary">Show</a>
                        {{-- <a href="/basket/{{ $produ ct->id }}" class="btn btn-success">Añadir +  a la cesta</a> --}}

                        <td><a href="#" class="btn btn-primary">Borrar campo</a></td>
                        <?php $total2 += $total ?>
                        <tr>
                          @endif
                          @endforeach
                          @empty
                          <td colspan="4">No hay products!!</td></tr>
                          @endforelse
                          <td>Total:  {{ $total2  }} €</td>



                        </tbody>
                      </table>
                      <a href="/basket/flush" name="borrar">Borrar lista</a>
                      <a href="/products" class="btn btn-primary">Volver</a>
                      {{-- <a href="/basket/store" class="btn btn-primary">Guardar cesta como pedido</a> --}}
                      @if($products != null)
                      <form method="post" action="/basket">
                        {{ csrf_field() }}
                        <input type="submit" name="" value="confirmar compra" class=" btn btn-danger">
                      </form>
                      @endif
                    </div>
                  </div>
                </div>
                @endsection
