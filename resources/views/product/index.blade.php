@extends('layouts.app')

@section('content')
<h1>Lista de productos</h1>
<a href="/products/create" class="btn btn-primary">Añadir nuevo producto</a>
<table class="table table-hover">
    <tr>
        <td><b>Nombre</td>
            <td><b>Precio</td>
                <td><b>Categoría</td>
                    <td></td>
                </tr>
                @forelse ($products as $product)
                @foreach ($categories as $category)
                @if($product->cathegory_id == $category->id)
                <tr>
                    <td> {{ $product->name}} </td>
                    <td> {{$product->price}} </td>
                    <td> {{ $category->name}} </td>
                    <td>
                        <a href="/products/{{$product->id}}/edit" class="btn btn-primary">Editar</a>
                        <form method="post" action="/products/{{ $product->id }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Borrar">
                        </form>
                        <a href="/products/{{$product->id}}" class="btn btn-primary">Show</a>
                        <a href="/basket/{{ $product->id }}" class="btn btn-success">Añadir a la cesta</a>
                    </td>
                </tr>
                @endif
                @endforeach
                @empty
                -No hay productos
                @endforelse
            </table>
            @endsection

