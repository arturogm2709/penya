@extends('layouts.app')

@section('title', 'Productos')

@section('content')
<h1>Editar Producto</h1>

<form method="post" action="/products/{{ $product->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT">

    <label>Nombre Producto</label>
    <input type="text" name="name" value="{{ old('name') }}">
    <div class="alert alert-danger">
        {{ $errors->first('name') }}
    </div>
    <br>
    <label>Precio</label>
    <input type="text" name="price" value="{{ old('price') }}">
    <div class="alert alert-danger">
        {{ $errors->first('price') }}
    </div>
    <br>
    <label>Nombre</label>
    <select name="cathegory_id">
        @foreach ($categories as $category)
        <option value="{{ $category->id }}"
            {{ old('cathegory_id') == $category?
            'selected="selected"' :
            ''
        }}>{{ $category->name }}
    </option>
    @endforeach
    <div class="alert alert-danger">
        {{ $errors->first('cathegory_id') }}
    </div>
</select>

<input type="submit" value="Guardar Cambios">
</form>
@endsection
