@extends('layouts.app')

@section('content')

<div class="card-body">
    <h1>Producto {{ $product->name }}</h1>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Precio</td>
                <td>Categoria</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{-- lo mismo que en categoria, buscamos la categoria a la que pertenece el producto y la mostramos --}}
                    @foreach ($categories as $category)
                    @if($product->cathegory_id == $category->id)
                    {{ $category->name}}
                    @endif
                    @endforeach
                </td>
            </tr>
        </tbody>
    </table>
    <a href="/products" class="btn btn-primary">Volver</a>
</div>

@endsection


