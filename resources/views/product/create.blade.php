@extends('layouts.app')

@section('title', 'Categorias')

@section('content')
<style type="text/css">
.alert {
  padding: 5px;
  background-color: #faa; /* Red */
  margin: 5px;
}
</style>
<h1>Crear productos:</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
    {{--
        --}}

        @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
        @endif

        <form method="post" action="/products">
            {{ csrf_field() }}

            <label>Nombre Producto</label>
            <input type="text" name="name" value="{{ old('name') }}">
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
            <br>
            <label>Precio</label>
            <input type="text" name="price" value="{{ old('price') }}">
            <div class="alert alert-danger">
                {{ $errors->first('price') }}
            </div>
            <br>
            <label>Id categoria</label>
            <select name="cathegory_id">
                @foreach ($categories as $category)
                <option value="{{ $category->id }}"
                    {{ old('cathegory_id') == $category?
                    'selected="selected"' :
                    ''
                }}>{{ $category->name }}
            </option>
            @endforeach
            <div class="alert alert-danger">
                {{ $errors->first('cathegory_id') }}
            </div>
        </select>
        <br>

        <input type="submit" value="Nuevo">
    </form>
    @endsection
