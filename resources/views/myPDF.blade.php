<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <title></title>
</head>
<body>

  <h2> PEDIDO de {{$order->user->name}} </h2>

  <h4>{{ $order->date->format('d-m-Y') }}</h4>

  <ul>Estado del pedido
    @if($order->paid == 0)
    <li>No pagado</li>
    @endif
    @if($order->paid != 0)
    <li>Pagado</li>
    @endif
  </ul>
  <ul>Fecha actualización
    <li>{{ $order->updated_at }}</li>
  </ul>

  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Cantidad de productos</th>
        <th>Total por producto</th>
      </tr>
    </thead>
    {{ $order->date }}
    <tbody>
      @foreach ($order->products as $product)
      <tr>
        <td>{{ $product->id }}</td>
        <td>{{ $product->name }}</td>
        <td style="text-align: right;">{{ $product->price }}€</td>
        <td style="text-align: right;">{{ $product->pivot->quantity }}</td>
        <td style="text-align: right;">{{ $product->price * $product->pivot->quantity }}€</td>
      </tr>
      @endforeach
      <tr>
        <td colspan="2" >El total del pedido es : </td><td></td><td></td><td style="text-align: right;"><strong>{{ $order->total($order->id) }}€</strong></td>
      </tr>
    </tbody>
  </table>
  <h5></h5>

</body>
</html>
