<h2> Pedido {{$order->user->name}}: </h2>


<table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Precio</th>
    </tr>
</thead>
{{ $order->date->format('d-m-Y') }}
<tbody>
  @forelse ($order->products as $product)
  <tr>
    <td>{{ $product->id }}</td>
    <td>{{ $product->name }}</td>
    <td>{{ $product->price }}</td>
    @empty
    @endforelse
</tr>
{{-- {{ $order->date }} --}}
</tbody>
</table>
