@extends('layouts.app')

@section('content')

<div class="card-body">
    <h1>Datos de la categoría: {{ $category->name }}</h1>{{-- sacamos nombre de la categoría --}}
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td><b>Nombre</b></td>
                <td><b>ID</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $category->name }}</td>{{-- nombre y numero id --}}
                <td>{{ $category->id }}</td>
            </tr>
            <tr>
                <td><h3>Productos</h3></td>
            </tr>
            <tr>
                <td><b>Nombre de producto</b></td>
                <td><b>Precio</b></td>
            </tr>
            <?php $contador = 0; ?>{{-- contador a 0 para sacar la cantidad de productos --}}
            @foreach($products as $product){{-- recorremos productos y miramos si coincide la categoría con la que buscamos --}}
            @if($product->cathegory_id == $category->id)
            <?php $contador++; ?>
            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
            </tr>
            @endif
            @endforeach
            @if($contador == 0){{-- si contador sigue a 0 , no hay productos de esa cateogria --}}
            <tr>
                <td><h2>No hay productos asignados a esta categoria</h2></td>
            </tr>
            @endif
        </tbody>
    </table>
    <a href="/category" class="btn btn-primary">Volver</a>
</div>
@endsection
