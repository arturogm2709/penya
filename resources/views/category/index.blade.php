@extends('layouts.app')

@section('content')




<div class="card-header"><h1>Categorias</h1><br>
  <a href="/category/create" class="btn btn-primary">Nueva categoria</a>
  <div class="card-body">
    <table class="table table-hover">
      <thead>
        <tr>
          <td><h4><b> Nombre </h4></td>
            <td><h4><b>Identificador de categoría</h4></td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            @forelse ($categories as $category)
            <tr>
             <td>{{ $category->name}}</td>
             <td>{{$category->id}}</td>
             <td>
               <a href="/category/{{ $category->id }}/edit" class="btn btn-primary">Editar</a>
               <a href="/category/{{ $category->id }}" class="btn btn-primary">Ver</a>
               <form method="post" action="/category/{{ $category->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
              </form>
            </td>
          </tr>

          @empty
          <h1><b>No hay categorias</h1>
            @endforelse
          </tbody>
        </table>
        {{ $categories->render() }}

      </div>
    </div>
    @endsection
