@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Grupo de usuarios en sesion</h1>

      <table  class="table table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email>
              <th>Role</th>
              <th>Cantidad</th>
            </tr>
          </thead>


          <tbody>


            @forelse ($users as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->role->name }}</td>
              <td>{{ $user->cantidad }}</td>
              <td></td>


           <tr>
            @empty
          <td colspan="4">No hay usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>
    <a href="/groups/flush" name="borrar">Borrar lista</a>

  </div>
</div>
</div>
@endsection
