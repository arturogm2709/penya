@extends('layouts.app')

@section('content')

<div class="card-body">
    <h1>Estamos en el show de Pedido {{ $pedido->id }}</h1>
    <table class="table table-striped table-hover">
        <thead>
{{--             <tr>
                <td>Pagado??</td>
                <a class="btn btn-primary">Si</a>
                <a class="btn btn-primary">No</a>
            </tr> --}}

            {{-- {{ $pedido->products->name }} --}}


        </thead>
        <tbody>

                @foreach($productos as $producto)
                 <tr>
                <td>{{ $producto->name }}</td>
                <td>{{ $producto->price }}€</td>
                <td>{{ $producto->pivot->quantity }}</td>
                </tr>

                @endforeach
                <td>Precio total del pedido:  {{ $pedido->total($pedido->id) }}</td>
        </tbody>

    </table>
    @can ('update' , $pedido)
    <td><a class="btn btn-primary"  role="button"
        href="/order/{{ $pedido->id }}/edit">
        Editar
    </a>
</td>
<td><a class="btn btn-primary"  role="button"
        href="/order/{{ $pedido->id }}/generatePDF">
        Generar PDF
    </a>
</td>
@endcan

<a href="/order" class="btn btn-primary">Volver</a>
</div>

@endsection


