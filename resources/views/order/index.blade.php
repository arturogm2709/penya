@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de pedidos</h1>

      <table  class="table table-hover">
        <thead>
          <tr>
            <th><b>ID de pedido:</th>
              <th><b>Pagado/No pagado</th>
                <th><b>Fecha</th>
                  <th><b>Usuario</th>
                    <th><b>Creado en</b>
                      <th><b>Actualizado en</th>
                        {{-- <th><b>Precio de pedido</td> --}}
                        </tr>
                      </thead>


                      <tbody>


                        @forelse ($pedidos as $pedido)
                        @can ('view' , $pedido)

                        <tr>
                          <td>{{ $pedido->id }}</td>

                          @if($pedido->paid == 0)
                          <td>No pagado</td>
                          @endif
                          @if($pedido->paid != 0)
                          <td>Pagado</td>
                          @endif
                          <td>{{ $pedido->date }} </td>
                          <td>{{ $pedido->user->name }}</td>
                          <td>{{ $pedido->created_at }}</td>
                          <td>{{ $pedido->updated_at }}</td>
                          {{-- <td>{{ $pedido->total }}</td> --}}

                          <td>

                            <a href="/order/{{$pedido->id}}" class="btn btn-primary">Show</a>
                          </td>
                          @can ('update' , $pedido)
                          <td><a class="btn btn-primary"  role="button"
                            href="/order/{{ $pedido->id }}/edit">
                            Editar
                          </a>
                        </td>
                        <td>
                          <form method="post" action="/order/{{ $pedido->id }}">

                            {{ csrf_field() }}

                            @can('delete', $pedido)
                            <input type="hidden" name="_method" value="DELETE">
                            <input  type="submit" value="borrar" class="btn btn-primary">
                            @endcan
                          </form>
                        </td>
                      </td>
                      @endcan

{{--                         @can ('update' , $pedido)
                        <td><a class="btn btn-primary"  role="button"
                          href="/order/{{ $pedido->id }}/delete">
                          Eliminar pedido
                        </a>
                      </td>
                      @endcan --}}
                      <tr>
                        @endcan
                        @empty
                        <td colspan="4">No hay pedidos!!</td></tr>
                        @endforelse

                        {{-- <td>Total:  {{ $total2  }} €</td> --}}

                      </tbody>
                    </table>
                     {{--  <a href="/basket/flush" name="borrar">Borrar lista</a>
                     <a href="/products" class="btn btn-primary">Volver</a> --}}
                     {{-- <a href="/basket/store" class="btn btn-primary">Guardar cesta como pedido</a> --}}
{{--                       <form method="post" action="/order">
                        {{ csrf_field() }}
                        <input type="submit" name="" value="confirmar compra" class=" btn btn-danger">
                      </form> --}}
                    </div>
                  </div>
                </div>
                @endsection
