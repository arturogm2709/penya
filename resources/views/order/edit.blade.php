@extends('layouts.app')

@section('content')

<div class="card-body">
    <h1>Estamos en el Edit de pedido {{ $pedido->id }}</h1>

        <form method="post" action="/order/{{ $pedido->id }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">

                <label>Pagado</label>
                <select name="paid">
                    <option value="1">Si</option>
                    <option value="0">No</option>
                    {{-- @endforeach --}}
                </select>
        <input type="submit" value="Guardar Cambios">
    </form>
    <a href="/order" class="btn btn-primary">Volver</a>
</div>

@endsection


